package com.epam.spring;

import com.epam.spring.logger.EventLogger;
import com.epam.spring.logger.EventType;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

public class App {

    private EventLogger defaultLogger;

    private Map<EventType, EventLogger> loggers;

    private Client client;

    public App(Client client, Map<EventType, EventLogger> loggers) {
        this.client = client;
        this.loggers = loggers;
    }

    public EventLogger getDefaultLogger() {
        return defaultLogger;
    }

    public void setDefaultLogger(EventLogger defaultLogger) {
        this.defaultLogger = defaultLogger;
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("context.xml");
        ctx.registerShutdownHook();

        App app = (App) ctx.getBean("app");
        EventType type;
        for (int i = 0; i < 15; i++) {
            if (i % 2 == 0) {
                type = EventType.INFO;
            } else if (i % 3 == 0) {
                type = EventType.ERROR;
            } else {
                type = null;
            }
            Event event = (Event) ctx.getBean("event");
            app.logEvent(type, event, "Some event for " + 1 + ", number: " + i);
        }
    }

    private void logEvent(EventType type, Event event, String msg) {
        EventLogger logger = null;
        if (type != null) {
            logger = loggers.get(type);
        }
        if (logger == null) {
            logger = defaultLogger;
        }
        String message = msg.replaceFirst(client.getId(), client.getFullName());
        event.setMsg(message + ", message type=" + type);
        logger.logEvent(event);
    }
}