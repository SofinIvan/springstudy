package com.epam.spring;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
public class Client {
    private String id;
    private String fullName;
    private String greeting;

    public Client(String id) {
        this.id = id;
    }
}