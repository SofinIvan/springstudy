package com.epam.spring.logger;

import com.epam.spring.Event;

public interface EventLogger {
    void logEvent(Event event);
}