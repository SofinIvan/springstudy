package com.epam.spring.logger;

import com.epam.spring.Event;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

@Getter
@Setter
public class FileEventLogger implements EventLogger {

    private String fileName;

    private File file;

    public FileEventLogger(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void logEvent(Event event) {
        try {
            FileUtils.writeStringToFile(file, event.getMsg() + "\n", true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void init() throws IOException {
        file = new File(fileName);
        if (!file.canWrite()) {
            throw new IOException("Can not write in specified file " + fileName);
        }
    }
}