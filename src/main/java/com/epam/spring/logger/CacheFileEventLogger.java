package com.epam.spring.logger;

import com.epam.spring.Event;
import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CacheFileEventLogger extends FileEventLogger {

    private static final int defaultCacheSize = 10;

    private int cacheSize;

    private List<Event> cache;

    public CacheFileEventLogger(String fileName) {
        this(fileName, defaultCacheSize);
    }

    public CacheFileEventLogger(String fileName, int cacheSize) {
        super(fileName);
        this.cacheSize = cacheSize;
        cache = new ArrayList<>(cacheSize);
    }

    @Override
    public void logEvent(Event event) {
        cache.add(event);
        if (cache.size() == cacheSize) {
            writeAndClearBuffer();
        }
    }

    public void destroy() {
        if (!cache.isEmpty()) {
            writeAndClearBuffer();
        }
    }

    private void writeAndClearBuffer() {
        writeToFile();
        cache.clear();
    }

    private void writeToFile() {
        try {
            FileUtils.writeStringToFile(getFile(), getCacheContent(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getCacheContent() {
        StringBuilder builder = new StringBuilder();
        for (Event event : cache) {
            builder.append(event.getMsg()).append("\n");
        }
        return builder.toString();
    }
}