package com.epam.spring;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.text.DateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.Random;

@Getter
@Setter
@ToString(exclude = "df")
public class Event {
    private int id;

    private String msg;

    private Date date;

    private Random random;

    private DateFormat df;

    public Event(Date date, DateFormat df) {
        random = new Random();
        id = random.nextInt(1000);
        this.date = date;
        this.df = df;
    }

    public static boolean isDay() {
        final int hour = LocalTime.now().getHour();
        return hour > 8 && hour < 17;
    }
}